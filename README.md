# Bi_ACSNano_16_18658_2022

Contains input files for the article:

Co-Ion Desorption as the Main Charging Mechanism in Metallic 1T-MoS2 Supercapacitors

Sheng Bi, and Mathieu Salanne,
*ACS Nano*, 16, 18658, 2022

https://doi.org/10.1021/acsnano.2c07272 ([see here for a preprint](https://chemrxiv.org/engage/chemrxiv/article-details/62dabf4ca7d17e05ff67ea5a))

The folder *slit* contains input files for the slit electrode geometry for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The folder *slab* contains input files for the slab electrode geometry
